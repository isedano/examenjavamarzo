
<%@page import="model.Book"%>
<%@page import="java.util.Iterator"%>
<%@page import="java.util.ArrayList"%>
<jsp:useBean id="books" class="java.util.ArrayList" scope="request"/>  


<!DOCTYPE html>
<%@include file="/WEB-INF/view/header.jsp" %>

<div id="content">
    <h1>Lista de Libros</h1>
    <p>
        <a href="<%= request.getContextPath()%>/book/create">Nuevo libro</a>
    </p>
    <table>
        <tr>
            <th>Id</th>
            <th>Titulo</th>
            <th>Paginas</th> 
        </tr>
        <%        Iterator<model.Book> iterator = books.iterator();
            while (iterator.hasNext()) {
                Book book = iterator.next();%>
        <tr>
            <td><%= book.getId()%></td>
            <td><%= book.getTitle()%></td>
            <td><%= book.getPages()%></td>
 
            <td> 
                <a href="<%= request.getContextPath() + "/book/view/" + book.getId()%>"> Ver </a>
            </td>
        </tr>
        <%
            }
        %>          
    </table>
    <a href="<%= request.getContextPath() + "/book/index/1" %>"> 1</a>
    <a href="<%= request.getContextPath() + "/book/index/2" %>"> 2</a>
    <a href="<%= request.getContextPath() + "/book/index/3" %>"> 3</a>
    <a href="<%= request.getContextPath() + "/book/index/4" %>"> 4</a>
    <a href="<%= request.getContextPath() + "/book/index/5" %>"> 5</a>
    <a href="<%= request.getContextPath() + "/book/index/6" %>"> 6</a>
    <br>

   
</div>
<%@include file="/WEB-INF/view/footer.jsp" %>