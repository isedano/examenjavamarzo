/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package persistence;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.logging.Level;
import model.Book;

/**
 *
 * @page alumno
 */
public class BookDAO  extends BaseDAO{
    public BookDAO() {
//        Class.forName("com.mysql.jdbc.Driver");
//        this.connection = DriverManager.getConnection(DB_URL, DB_USER, DB_PASS);
    }
    public ArrayList<Book> getAll(int page, int elementsPerPage) {
        PreparedStatement stmt = null;
        ArrayList<Book> books = null;
        int offset = (page - 1) * elementsPerPage;
        
        try {
            this.connect();
            stmt = connection.prepareStatement("select * from books LIMIT " + elementsPerPage + " OFFSET " + offset);
            ResultSet rs = stmt.executeQuery();
            books = new ArrayList();

            int i = 0;
            while (rs.next()) {
                i++;
                Book book = new Book();
                book.setId(rs.getInt("id"));
                book.setTitle(rs.getString("title"));
                book.setPages(rs.getInt("pages"));
                book.setYear(rs.getInt("year"));

                books.add(book);
                LOG.info("Registro fila: " + i);
            }
            this.disconnect();
        } catch (SQLException ex) {
            LOG.log(Level.SEVERE, null, ex);
        }
        return books;
    }
 public ArrayList<Book> getPages(int page) {
        int size = 15;
        PreparedStatement stmt = null;
        ArrayList<Book> books = null;
        try {
            this.connect();
            stmt = connection.prepareStatement("select * from books limit ? offset ? ");
            stmt.setInt(1, size);
            stmt.setInt(2, (page - 1) * size);

            ResultSet rs = stmt.executeQuery();
            books = new ArrayList();

            int i = 0;
            while (rs.next()) {
                i++;
                Book book = new Book();
                book.setId(rs.getInt("id"));
                book.setPages(rs.getInt("pages"));
                book.setTitle(rs.getString("title"));
                book.setYear(rs.getInt("year"));

                books.add(book);
                LOG.info("Registro fila: " + i);
            }
            this.disconnect();
        } catch (SQLException ex) {
            LOG.log(Level.SEVERE, null, ex);
        }
        return books;
    }

   
     public Book get(long id) throws SQLException {
        LOG.info("get(id)");
        Book book = new Book();
        this.connect();
        PreparedStatement stmt = connection.prepareStatement("select * from books where id = ? ");
     
        stmt.setLong(1, id);
        ResultSet rs = stmt.executeQuery();
        
        LOG.info("consulta hecha");
        if (rs.next()) {
            LOG.info("Datos ...");
             book.setId(rs.getInt("id"));
             book.setTitle(rs.getString("title"));
            book.setPages(rs.getInt("pages"));
            book.setYear(rs.getInt("year"));
            LOG.info("Datos cargados");
        } else {
            LOG.log(Level.INFO, "No hay datos para el id {0}", id);
        }
        this.disconnect();
        return book;
    }
    public void insert(Book book) {
        PreparedStatement stmt = null;
        try {
            this.connect();
            stmt = connection.prepareStatement("INSERT INTO books(title, pages, year) VALUES(?, ? ,?)");
            stmt.setInt(1, book.getPages());
            stmt.setString(2, book.getTitle());
            stmt.setInt (3, book.getYear());
            
            
            stmt.execute();
            this.disconnect();
        } catch (SQLException ex) {
            LOG.log(Level.SEVERE, null, ex);
        }
        return;        
    }

    public void store(Book book) throws SQLException {
        PreparedStatement stmt = null;
        LOG.info("Crear DAO");
        this.connect();
        stmt = connection.prepareStatement(
                "INSERT INTO books(title, pages, year)"
                + " VALUES(?, ?, ?)"
        );
        //stmt.setLong(1, 4); // teacher.getId()
        
        //LOG.info(""+teacher.getId());
        
        stmt.setString(1, book.getTitle());
        stmt.setInt(2, book.getPages());
        stmt.setInt(3, book.getYear());

        stmt.execute();
        this.disconnect();
    }


}
